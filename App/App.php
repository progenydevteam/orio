<?php 
namespace App;

use League\{
	Container\Container,
	Route\RouteCollection
};

use Zend\{
	Diactoros\Response,
	Diactoros\ServerRequestFactory,
	Diactoros\Response\SapiEmitter
};

use Doctrine\{
	ORM\Tools\Setup,
	ORM\EntityManager
};

use Config\Config;

use App\Controllers\MainController;

class App{
	public static $get;

	private $entityManager;

	public function __construct(bool $routing = true){
		App::$get = $this;
		$this->initDatabase();
		if($routing){
			$this->initRouting();
		}
	}

	public function initDatabase(){
		$db_configuration = Setup::createAnnotationMetadataConfiguration([ROOT . 'App/Entities/'], Config::DEV_MODE);
		$db_connection = array(
			'driver' => 'pdo_sqlite',
			'path' => ROOT . 'App/Database/' . Config::DB_NAME . '.sqlite',
		);
		$this->entityManager = EntityManager::create($db_connection, $db_configuration);
	}

	public function initRouting(){
		$container = new Container;
		$container->share('response', Response::class);
		$container->share('request', function () {
			return ServerRequestFactory::fromGlobals(
				$_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
			);
		});
		$container->share('emitter', SapiEmitter::class);
		$router = new RouteCollection($container);
		$router->addPatternMatcher('path', '.*');
		$router->map(['GET', 'POST'], '/phpliteadmin.php', function(){
			include(ROOT . 'App/Database/phpliteadmin.php');
		});
		$router->map(['GET', 'POST'], '/', [new MainController, 'dispatch']);
		$router->map(['GET', 'POST'], '/{route:path}', [new MainController, 'dispatch']);
		$response = $router->dispatch($container->get('request'), $container->get('response'));
		$container->get('emitter')->emit($response);
	}

	public function getEntityManager(){
		return $this->entityManager;
	}
}