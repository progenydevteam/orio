<?php
namespace App\Entities;
/**
 * @Entity @Table(name="BasicEntities")
 **/
class BasicEntity {
	/** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $string;

    public function getId(){
        return $this->id;
    }

    public function getString(){
    	return $this->string;
    }

    public function setString($string){
    	$this->string = $string;
    }
}