<?php
namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use League\Plates\Engine;

abstract class Controller{
	protected $templates;

	public function __construct() {
		$this->templates = new Engine(ROOT . 'App/Templates/');
	}

	public function dispatch(ServerRequestInterface $request, ResponseInterface $response, array $args) {
		$this->response = $response;
		$router = new \League\Route\RouteCollection;

		$this->initRoutes($router);
		
		$status = 200;

		try{
			$response = $router->dispatch($request, $response);
		} catch(\League\Route\Http\Exception\NotFoundException $e) {
			$response->getBody()->write($this->render('NotFoundTemplate', ['content' => $args['route']]));
			$status = $e->getStatusCode();
		}

		return $response->withStatus($status);
	}

	public function getHandler($method){
		return function(ServerRequestInterface $request, ResponseInterface $response, array $args) use ($method) {
			$handlerResponse = call_user_func_array([$this, $method], [
				$request, 
				$response,
				$args
			]);
			$response->getBody()->write($handlerResponse);
			return $response;
		};
	}
	
	public function render($name, array $data) {
		return $this->templates->render($name, $data);
	}
}