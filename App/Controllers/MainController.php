<?php 
namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class MainController extends Controller implements ControllerInterface{
	public function getPath(){
		return '/';
	}

	public function initRoutes(\League\Route\RouteCollection $router){
		$router->group($this->getPath(), function($route) {
			$route->get('/', $this->getHandler('executeTest'));
		});
	}

	public function executeTest() {
		$entity = new \App\Entities\BasicEntity();
		$entity->setString('wow');
		\App\App::$get->getEntityManager()->persist($entity);
		\App\App::$get->getEntityManager()->flush();
		return $this->render('TestTemplate', ['content' => 'world!']);
	}
}