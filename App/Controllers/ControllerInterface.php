<?php
namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ControllerInterface {
	public function dispatch(ServerRequestInterface $request, ResponseInterface $response, array $args) ;
	public function getPath();
	public function initRoutes(\League\Route\RouteCollection $router);
}