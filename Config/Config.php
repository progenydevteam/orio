<?php
namespace Config;

class Config {
	public const DEV_MODE = true;
	public const DB_NAME = "orio";
	public const DB_PWD = "$2y$10\$tpieD60PyoSptthOqvmYFusgI3AFMhiikRYaiPe70bYk0WgXBJuCe";
}