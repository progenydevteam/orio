<?php
use App\App;

use Doctrine\ORM\Tools\Console\ConsoleRunner;

include(__DIR__ . '/../vendor/autoload.php');

define('ROOT', __DIR__ . '/../');

$app = new App(false);

$entityManager = $app->getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);